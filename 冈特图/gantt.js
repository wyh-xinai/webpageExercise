

// 时间加法计算
function dateAddTime(date, time){
    return new Date(new Date(date).getTime()+new Date(time).getTime());
}


//对json字符串进行格式化，options可以去掉
var formatJson = function (json, options) {
    var reg = null,
            formatted = '',
            pad = 0,
            PADDING = '    '; // one can also use '\t' or a different number of spaces
    // optional settings
    options = options || {};
    // remove newline where '{' or '[' follows ':'
    options.newlineAfterColonIfBeforeBraceOrBracket = (options.newlineAfterColonIfBeforeBraceOrBracket === true) ? true : false;
    // use a space after a colon
    options.spaceAfterColon = (options.spaceAfterColon === false) ? false : true;

    // begin formatting...

    // make sure we start with the JSON as a string
    if (typeof json !== 'string') {
        json = JSON.stringify(json);
    }
    // parse and stringify in order to remove extra whitespace
    json = JSON.parse(json);
    json = JSON.stringify(json);

    // add newline before and after curly braces
    reg = /([\{\}])/g;
    json = json.replace(reg, '\r\n$1\r\n');

    // add newline before and after square brackets
    reg = /([\[\]])/g;
    json = json.replace(reg, '\r\n$1\r\n');

    // add newline after comma
    reg = /(\,)/g;
    json = json.replace(reg, '$1\r\n');

    // remove multiple newlines
    reg = /(\r\n\r\n)/g;
    json = json.replace(reg, '\r\n');

    // remove newlines before commas
    reg = /\r\n\,/g;
    json = json.replace(reg, ',');

    // optional formatting...
    if (!options.newlineAfterColonIfBeforeBraceOrBracket) {
        reg = /\:\r\n\{/g;
        json = json.replace(reg, ':{');
        reg = /\:\r\n\[/g;
        json = json.replace(reg, ':[');
    }
    if (options.spaceAfterColon) {
        reg = /\:/g;
        json = json.replace(reg, ': ');
    }

    $.each(json.split('\r\n'), function (index, node) {
        var i = 0,
                indent = 0,
                padding = '';

        if (node.match(/\{$/) || node.match(/\[$/)) {
            indent = 1;
        } else if (node.match(/\}/) || node.match(/\]/)) {
            if (pad !== 0) {
                pad -= 1;
            }
        } else {
            indent = 0;
        }

        for (i = 0; i < pad; i++) {
            padding += PADDING;
        }

        formatted += padding + node + '\r\n';
        pad += indent;
    });

    return formatted;
};

// 用于把数据项目，插入到ul中
function showDataInUl(data){
    let lis = "";
    for(let project_name in data){
        let template = `
        <li>
            <span class="name">${project_name}</span>
            <span class="edit" project_name="${project_name}">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </span>
            <span class="delete" project_name="${project_name}">
                <i class="fa fa-trash" aria-hidden="true"></i>
            </span>
        </li>
        `;
        lis += template;
    }
    $(".manage-wrap .ul-wrap ul").html(lis);
}

// 根据自定义的data生成e-charts要的option
var mackOption = function(data, title) {
    if( !data || data == {}){
        return {};
    }
    title = title || "项目计划甘特图";
    
    // console.log("mackOption() start!");
    //  Y轴上的项目名称
    let yAxis_data = [];
    for(let project_name in data){
        yAxis_data.push(project_name);
    }
    yAxis_data.reverse();
    // console.log("项目名称: ", yAxis_data);

    // 图例中的所有各类数据名称（只插入要显示的数据，不显示的不要插入）
    let legend_data = [];
    // 最后echarts要求的数据格式
    let series = [];
    // 用于辅助的数据列前缀，可根据数据情况进行修改
    let prefix = "辅助-";
    for(let i in yAxis_data){
        let project_name = yAxis_data[i];
        for(let legend in data[project_name]){
            if(legend_data.indexOf(legend)<0){
                legend_data.push(legend);
                // 透明的部分
                series.push(
                    {
                        name: prefix + legend,
                        type: "bar",
                        stack: legend,
                        itemStyle: {
                            barBorderColor: "#00000000",
                            color: "#00000000"
                        },
                        emphasis: {
                            itemStyle: {
                                barBorderColor: "#00000000",
                                color: "#00000000"
                            }
                        },
                        data: []
                    },
                );
                // 彩色的部分
                series.push(
                    {
                        name: legend,
                        type: "bar",
                        stack: legend,
                        barGap: "-100%",
                        data: []
                    },
                );
                
            }
        }
    }
    // console.log("各类数据名称: ",legend_data);
    // console.log("初始化后的空数据: ",series);

    //定义一个方法快速根据name属性插入data
    function pushDataByName(series, name, data){
        for(let i in series){
            // console.log(series[i].name, name);
            if(series[i].name === name){
                series[i].data.push(data);
                return;
            }
        }
    }

    // 遍历data数组，确保所有输入数据都进行处理。
    for(let i in yAxis_data){
        let project_name = yAxis_data[i];
        // 遍历legend_data所有属性，如果plan中有该值，则填充该值，如果没有，就填充"-"
        for(let j in legend_data){
            let legend = legend_data[j];
            // 如果有值
            if(data[project_name][legend]){
                pushDataByName(series, prefix + legend, new Date(data[project_name][legend][0]));
                
                if(!data[project_name][legend][1]){
                    // 如果没有结束日期，插入今天日期
                    data[project_name][legend].push(echarts.format.formatTime('yyyy-MM-dd', new Date()));
                }
                pushDataByName(series, legend, new Date(data[project_name][legend][1]) - new Date(data[project_name][legend][0]))
                ;
            }else{
                pushDataByName(series, prefix + legend, "-");
                pushDataByName(series, legend, "-");
            }

        }
    }

    // 定一一个option的模板，然后把刚才计算获得各个属性赋值进去。
    let option_template = {
        animation: true,
        title: {
            text: title,
            top: 50,
            left:150,
            textStyle: {
                fontSize: 30,
                fontWeight: "bolder",
            }
        },
        legend: {
            // data：["计划工期", "可行性研究阶段", "初步设计阶段", "施工图设计阶段", "项目实施阶段", "项目验收阶段"]，
            align: "right",
            right: 100,
            top: 80,
            itemGap : 20,
        },
        grid: {
            containLabel: true,
            show: false,
            top:120,
            left:100,
            right:100,
            bottom: 80,
        },
        xAxis: {
            // 使用value才有累积效果
            type: "value",
            minorTick:{
                show: true
            },
            axisLabel: {
                show: true,
                interval:0,
                formatter: function (value, index) {
                    return echarts.format.formatTime('yyyy-MM-dd', value);
                }
            },
            axisPointer: {
                value: new Date(),
                snap: false,
                lineStyle: {
                    color: '#004E52',
                    opacity: 0.5,
                    width: 3
                },
                label: {
                    show: true,
                    formatter: function (params) {
                        return echarts.format.formatTime('yyyy-MM-dd', params.value);
                    },
                    backgroundColor: '#004E52'
                },
                handle: {
                    show: true,
                    color: '#004E52'
                }
            },
            splitLine: {
                show: true
            },
            min: "dataMin",
            max: "dataMax"
        },
        yAxis: {
            axisLabel: {
                show: true,
                interval: 'auto'
            },
            splitLine: {
                show: false
            },
            // data: ["第一个任务", "第二个任务", "第三个任务"]
        },
        tooltip: {
            trigger: 'axis',
            triggerOn: 'click',
            formatter: function(params) {
                var res = params[0].name + "</br>"
                // 获取所有seriesName和对应的index
                let seriesNames = {};
                for (let i in params){
                    seriesNames[params[i].seriesName]=parseInt(i);
                }

                for(let seriesName in seriesNames){
                    if( seriesName.indexOf(prefix)===0 ){
                        continue;
                    }else{
                        let i0 = seriesNames[prefix + seriesName];
                        let i1 = seriesNames[seriesName]
                        if(params[i0].data==="-" ){
                            // 如果该项目没有该类数据
                            res += seriesName + ": 无数据</br>"
                        }else{
                            // 如果该项目有该类数据
                            let date0 = echarts.format.formatTime('yyyy-MM-dd', params[i0].data);
                            let date1 = echarts.format.formatTime('yyyy-MM-dd', dateAddTime(date0,params[i1].data));
                            res += seriesName + ": " + date0 + " ~ " + date1 + "</br>"
                        }
                    }
                }
                return res;
            }
        },
    }

    option_template.legend.data = legend_data;
    option_template.yAxis.data = yAxis_data;
    option_template.series = series;
    
    // console.log("最后的得到的数据: ", series);
    showDataInUl(data);

    // console.log("将当前data数据保存至cookie中。 ");
    $.cookie('latestData', JSON.stringify(data), { expires: 365*10000 });
    // console.log(JSON.parse($.cookie('latestData')));

    // console.log("mackOption() completed!");
    return option_template;
}

// 定义一个重设echarts的方法
function resetMyChartByNewData(myChart, data, newData, title){
    try{
        if(typeof(newData)=='string'){
            // 字符串预处理，避免JSON解析错误
            newData = newData.replace(/,*\s*\}/g, "}");
            newData = JSON.parse(newData);
        }
        Object.assign(data, newData);
        // 清空当前实例，会移除实例中所有的组件和图表。
        myChart.clear();
        // 生成图表
        myChart.setOption(option = mackOption(data, title));
    }
    catch(err){
        alert("错误："+ err);
    }
}

// Js 实现文件生成并下载  
// by 戴向天
// ref https://blog.csdn.net/weixin_41088946/article/details/106682196
function download(content,fileName){
    const a = document.createElement('a');
    const event = document.createEvent("MouseEvents");
    const blob = new Blob([content]);
    a.href = URL.createObjectURL(blob)
    a.download = fileName
    event.initEvent('click', true, true);
    a.dispatchEvent(event)
}




